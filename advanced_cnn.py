import numpy as np
from keras import optimizers, losses
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Dense, Input, Flatten, Dropout, LSTM, Bidirectional
from keras.layers import Conv1D, MaxPooling1D, Embedding
from keras.models import Model
from keras.callbacks import EarlyStopping
import nn_utils
import loader
from nn_data_model import NNDataModel

data_percent = 1
# word_vec_model_path = "../pretrained_models/GoogleNews-vectors-negative300.bin"
word_vec_model_path = '../pretrained_models/glove.6B/glove.6B.100d.txt'
# word_vec_model_path = "models/both_comments_50"

serialized_train_data_path = "serialized_data/glove_100_convnet_train_prob_pp_change" + str(data_percent)
serialized_test_data_path = "serialized_data/glove_100_convnet_test_prob_pp_change" + str(data_percent)

EMBEDDING_DIM = 100  # how big is each word vector
MAX_VOCAB_SIZE = 410148  # how many unique words to use (i.e num rows in embedding vector)
MAX_SEQUENCE_LENGTH = 400  # max number of words in a comment to use

# training params
batch_size = 256
num_epochs = 50


def init():
    train_data, test_data = loader.load_data(process_comments=False, percent_of_data=data_percent,
                                             serialized_train_path=serialized_train_data_path,
                                             serialized_test_path=serialized_test_data_path)
    train_data, test_data = nn_utils.cast_data(train_data, test_data)

    if train_data.word_vector_embedding_weights is None:
        train_data, test_data = calculate_embeddings(train_data, test_data, model="glove")
    cnn(train_data, test_data)


def cnn(train_data: NNDataModel, test_data: NNDataModel):
    train_x = train_data.get_comments_vector()
    test_x = test_data.get_comments_vector()

    train_y = to_categorical(train_data.labels)
    test_y = to_categorical(test_data.labels)

    model = YoonKimNet(train_data.word_vector_embedding_weights, MAX_SEQUENCE_LENGTH,
                         len(train_data.train_word_index) + 1, EMBEDDING_DIM, 2, False)

    early_stopping = EarlyStopping(monitor='val_loss', min_delta=0.01, patience=6, verbose=1)
    callbacks_list = [early_stopping]

    hist = model.fit(train_x, train_y, epochs=num_epochs,
                     callbacks=callbacks_list, validation_split=0.1, shuffle=True, batch_size=batch_size)
    nn_utils.plot(hist)

    loss, accuracy = model.evaluate(train_x, train_y, verbose=False)
    print("Training Accuracy: {:.4f}".format(accuracy))
    loss, accuracy = model.evaluate(test_x, test_y, verbose=False)
    print("Testing Accuracy:  {:.4f}".format(accuracy))

    # y_test = model.predict(test_cnn_data, batch_size=1024, verbose=1)


# def load_dataset_cnn(train_test_ratio):
#     # data = pandas.read_csv(dataset_path)
#     data = pandas.read_csv(
#         dataset_path,
#         header=0,
#         skiprows=lambda i: i > 0 and random.random() > data_percent
#     )
#
#     # data.to_csv("../dataset/data_" + str(data_percent)+ ".csv", encoding='utf-8')
#     data.fillna('_NA_')
#     data = standardize_text(data, "comment")
#     data = standardize_text(data, "parent_comment")
#     data.to_csv("train_clean_data.csv")
#     data.head()
#
#     train_set_size = int(data.size * train_test_ratio)
#
#     train_data = DataModel(comments=data["comment"][:train_set_size], labels=data["labels"][:train_set_size],
#                            score=None, ups=None, downs=None,parents=data["parent_comment"][:train_set_size])
#     test_data = DataModel(comments=data["comment"][train_set_size:], labels=data["labels"][train_set_size:],
#                            score=None, ups=None, downs=None,parents=data["parent_comment"][train_set_size:])
#
#     return train_data, test_data


def calculate_embeddings(train_data: NNDataModel, test_data: NNDataModel, model="word2vec"):

    if model == "glove":
        vector_model, MAX_VOCAB_SIZE = loader.load_glove_model(word_vec_model_path)
    else:
        vector_model, MAX_VOCAB_SIZE = loader.load_word2vec_model(word_vec_model_path, None)

    tokenizer = Tokenizer(num_words=MAX_VOCAB_SIZE, lower=True, char_level=False)
    tokenizer.fit_on_texts(train_data.get_parent_comments())
    tokenizer.fit_on_texts(train_data.get_comments())
    train_comments_sequences = tokenizer.texts_to_sequences(train_data.get_comments())
    train_parent_comments_sequences = tokenizer.texts_to_sequences(train_data.get_parent_comments())

    train_word_index = tokenizer.word_index
    print('Found %s unique tokens.' % len(train_word_index))

    train_comments_vector = pad_sequences(train_comments_sequences, maxlen=MAX_SEQUENCE_LENGTH)
    train_parent_comments_vector = pad_sequences(train_parent_comments_sequences, maxlen=MAX_SEQUENCE_LENGTH)

    train_embedding_weights = np.zeros((len(train_word_index) + 1, EMBEDDING_DIM))
    for word, index in train_word_index.items():
        train_embedding_weights[index, :] = vector_model[word] if word in vector_model else np.random.rand(EMBEDDING_DIM)
    print(train_embedding_weights.shape)

    # test_sequences = tokenizer.texts_to_sequences(clean_test_comments["comment_text"].tolist())
    test_comment_sequences = tokenizer.texts_to_sequences(test_data.get_comments())
    test_comments_vector = pad_sequences(test_comment_sequences, maxlen=MAX_SEQUENCE_LENGTH)
    test_parent_comment_sequences = tokenizer.texts_to_sequences(test_data.get_parent_comments())
    test_parent_comments_vector = pad_sequences(test_parent_comment_sequences, maxlen=MAX_SEQUENCE_LENGTH)

    train_data.set_comments_vector(train_comments_vector, train_parent_comments_vector)
    train_data.word_vector_embedding_weights = train_embedding_weights
    train_data.train_word_index = train_word_index

    test_data.set_comments_vector(test_comments_vector, test_parent_comments_vector)

    loader.save_data_model(train_data, serialized_train_data_path)
    loader.save_data_model(test_data, serialized_test_data_path)

    return train_data, test_data


def ConvLSTMNet(embeddings, max_sequence_length, num_words, embedding_dim, labels_index, trainable=False):
    embedded_sequences, sequence_input = create_embedding_layer(embeddings, max_sequence_length, num_words,
                                                                embedding_dim, trainable)

    l_conv = Conv1D(filters=64, kernel_size=5, activation='relu')(embedded_sequences)
    l_pool = MaxPooling1D(pool_size=4)(l_conv)
    lstm = LSTM(100)(l_pool)

    preds = Dense(labels_index, activation='softmax')(lstm)

    model = Model(sequence_input, preds)

    model.compile(optimizer=optimizers.Adagrad(lr=0.001),
                  loss=losses.categorical_crossentropy,
                  metrics=['accuracy'])

    model.summary()
    return model

def BiDirectLSTM(embeddings, max_sequence_length, num_words, embedding_dim, labels_index, trainable=False):
    embedded_sequences, sequence_input = create_embedding_layer(embeddings, max_sequence_length, num_words,
                                                                embedding_dim, trainable)
    #
    # l_conv = Conv1D(filters=64, kernel_size=5, activation='relu')(embedded_sequences)
    # l_pool = MaxPooling1D(pool_size=4)(l_conv)
    lstm = Bidirectional(LSTM(100))(embedded_sequences)

    preds = Dense(labels_index, activation='softmax')(lstm)

    model = Model(sequence_input, preds)

    model.compile(optimizer='rmsprop',
                  loss=losses.categorical_crossentropy,
                  metrics=['accuracy'])

    model.summary()
    return model


def create_embedding_layer(embeddings, max_sequence_length, num_words, embedding_dim, trainable=False):
    embedding_layer = Embedding(num_words,
                                embedding_dim,
                                weights=[embeddings],
                                input_length=max_sequence_length,
                                trainable=trainable)

    sequence_input = Input(shape=(max_sequence_length,), dtype='int32')
    return embedding_layer(sequence_input), sequence_input


def YoonKimNet(embeddings, max_sequence_length, num_words, embedding_dim, labels_index, trainable=False):

    embedded_sequences, sequence_input = create_embedding_layer(embeddings, max_sequence_length, num_words,
                                                                embedding_dim, trainable)

    # Yoon Kim model (https://arxiv.org/abs/1408.5882)
    convs = []
    filter_sizes = [3, 4, 5]

    for filter_size in filter_sizes:
        l_conv = Conv1D(filters=128, kernel_size=filter_size, activation='relu')(embedded_sequences)
        l_pool = MaxPooling1D(pool_size=2)(l_conv)
        convs.append(l_pool)

    conv = Conv1D(filters=128, kernel_size=3, activation='relu')(embedded_sequences)
    pool = MaxPooling1D(pool_size=3)(conv)

    x = Dropout(0.5)(pool)
    x = Flatten()(x)
    x = Dense(128, activation='relu')(x)
    x = Dropout(0.5)(x)

    preds = Dense(labels_index, activation='softmax')(x)

    model = Model(sequence_input, preds)
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['acc'])

    model.summary()
    return model


def ConvNet(embeddings, max_sequence_length, num_words, embedding_dim, labels_index, trainable=False):

    embedded_sequences, sequence_input = create_embedding_layer(embeddings, max_sequence_length, num_words,
                                                                embedding_dim, trainable)

    convs = []
    filter_sizes = [4, 3]

    for filter_size in filter_sizes:
        l_conv = Conv1D(filters=128, kernel_size=filter_size, activation='relu')(embedded_sequences)
        l_pool = MaxPooling1D(pool_size=2)(l_conv)
        convs.append(l_pool)

    x = Flatten()(l_pool)
    x = Dense(128, activation='relu')(x)

    preds = Dense(labels_index, activation='softmax')(x)

    model = Model(sequence_input, preds)

    model.compile(optimizer=optimizers.Adadelta(),
                  loss=losses.categorical_crossentropy,
                  metrics=['accuracy'])

    model.summary()
    return model


if __name__ == "__main__":
    #elmo embedding
    init()
