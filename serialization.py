import pickle


def serialize(obj, file_name):
    file = open(file_name, "wb")
    pickle.dump(obj, file)
    file.close()


def deserialize(file_name):
    file = open(file_name, "rb")
    obj = pickle.load(file)
    file.close()
    return obj
