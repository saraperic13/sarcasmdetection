import numpy as np


class DataModel(object):

    def __init__(self, comments, labels, score, ups, downs, parents, comments_vector=None, parents_vector=None):
        self.comments_vector = comments_vector
        self.labels = labels
        self.comments = comments
        self.score = score
        self.ups = ups
        self.downs = downs
        self.parents = parents
        self.parents_vector = parents_vector

    def set_comments_vector(self, comments_vector, parents_vector):
        self.comments_vector = comments_vector
        self.parents_vector = parents_vector

    def get_data(self):
        # return self.comments_vector
        # ups_vector = np.transpose(np.array([self.ups,]*20))
        # downs_vector = np.transpose(np.array([self.downs,]*20))
        # score_vector = np.transpose(np.array([self.score,]*20))
        # return np.column_stack((self.comments_vector, ups_vector, downs_vector,
        #                         score_vector))
        return np.column_stack((self.comments_vector, self.ups, self.downs,
                            self.score, self.parents_vector))

    def prepare_for_serialization(self):
        self.comments = None
        self.parents = None

    def get_both_comments_vector(self):
        # return self.comments_vector
        return np.column_stack((self.comments_vector, self.parents_vector))

    def get_comments(self):
        return self.comments
        # return np.column_stack((self.comments, self.parents))
