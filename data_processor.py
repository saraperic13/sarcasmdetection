import string

import numpy as np
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import nltk

# nltk.download()
stop_words = set(stopwords.words('english'))


def preprocess(data):
    result = []

    data.dropna()
    data = clean_text(data, "comment")
    data = clean_text(data, "parent_comment")

    for index, row in data.iterrows():
        if index % 1000 == 0:
            print("Removing stop words from comment %d of %d" % (index, len(data)))

        # Remove punctuation and convert to lower case
        # row['comment'] = str(row['comment']).translate(str.maketrans("", "", string.punctuation)).lower()
        # row['parent_comment'] = str(row['parent_comment']).translate(str.maketrans("", "", string.punctuation)).lower()

        # Remove stop words
        word_tokens = word_tokenize(str(row['comment']))
        comment = [w for w in word_tokens if w not in stop_words]

        word_tokens = word_tokenize(str(row['parent_comment']))
        parent = [w for w in word_tokens if w not in stop_words]

        if comment:
            result.append([row['label'], comment, row['score'], row['ups'], row['downs'], parent])

    return result


def feature_vec_method(words, model, num_features):
    feature_vec = np.zeros(num_features, dtype="float32")
    nwords = 0

    # Converting Index2Word which is a list to a set for better speed in the execution.
    index2word_set = set(model.wv.index2word)

    for word in words:
        if word in index2word_set:
            nwords = nwords + 1
            feature_vec = np.add(feature_vec, model[word])

    # Dividing the result by number of words to get average
    if nwords:
        feature_vec = np.divide(feature_vec, nwords)
    return feature_vec


def get_avg_feature_vectors(comments, model, num_features):
    counter = 0
    comment_feature_vectors = np.zeros((len(comments), num_features), dtype="float32")
    for comment in comments:

        if counter % 1000 == 0:
            print("Calculating average feature vectors of comment %d of %d" % (counter, len(comments)))

        comment_feature_vectors[counter] = feature_vec_method(comment, model, num_features)
        counter = counter + 1

    return comment_feature_vectors


def clean_text(df, text_field):
    df[text_field] = df[text_field].str.replace(r"http\S+", "")
    df[text_field] = df[text_field].str.replace(r"http", "")
    df[text_field] = df[text_field].str.replace(r"@\S+", "")
    df[text_field] = df[text_field].str.replace(r"[^A-Za-z0-9]", " ")
    df[text_field] = df[text_field].str.replace(r"@", "at")
    df[text_field] = df[text_field].str.lower()
    return df