from data_model import DataModel
import numpy as np


class NNDataModel(DataModel):

    def __init__(self, labels, comments=None, parents=None, score=None, ups=None,
                 downs=None, comments_vector=None, parents_vector=None, word_vector_embedding_weights=None,
                 train_word_index=None):
        super().__init__(comments=comments, parents=parents, labels=labels, ups=ups, downs=downs, score=score,
                         comments_vector=comments_vector, parents_vector=parents_vector)

        self.word_vector_embedding_weights = word_vector_embedding_weights
        self.train_word_index = train_word_index

    def get_comments(self):
        return self.comments

    def get_parent_comments(self):
        return self.parents

    def get_comments_vector(self):
        # return self.comments_vector
        return np.column_stack((self.comments_vector, self.parents_vector))
