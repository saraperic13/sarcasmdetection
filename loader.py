import os
import random
import numpy as np

import gensim
import pandas

import data_processor
import serialization
import word2vec
from data_model import DataModel

data_percent = 1  # % of whole dataset

serialized_train_data_path = "serialized_data/train_data_" + str(data_percent)
serialized_test_data_path = "serialized_data/test_data_" + str(data_percent)

# word2vec_model_path = "../pretrained_models/GoogleNews-vectors-negative300.bin"
word2vec_model_path = "models/both_comments_50"
# dataset_path = "../dataset/train-balanced-sarcasm.csv"
dataset_path = "dataset/data_0.1.csv"

num_features = 50  # Word vector dimensionality


def load_data(train_test_ratio=0.7, percent_of_data=data_percent, process_comments=True, serialized_train_path=serialized_train_data_path,
              serialized_test_path=serialized_test_data_path):
    train_data = load_data_model(serialized_train_path)
    test_data = load_data_model(serialized_test_path)

    if not train_data or not test_data:
        train_data, test_data = load_dataset(train_test_ratio, percent_of_data)

        if not process_comments:
            return train_data, test_data

        word2vec_model,_ = load_word2vec_model(word2vec_model_path, train_data.comments + test_data.comments
                                             + train_data.parents + test_data.parents)

        train_data.set_comments_vector(data_processor.get_avg_feature_vectors(train_data.comments,
                                                                              word2vec_model, num_features),
                                       data_processor.get_avg_feature_vectors(train_data.parents,
                                                                              word2vec_model, num_features))
        test_data.set_comments_vector(data_processor.get_avg_feature_vectors(test_data.comments,
                                                                             word2vec_model, num_features),
                                      data_processor.get_avg_feature_vectors(test_data.parents,
                                                                             word2vec_model, num_features))

        save_data_model(train_data, serialized_train_data_path)
        save_data_model(test_data, serialized_test_data_path)

    return train_data, test_data


def load_data_model(file_path):
    if os.path.isfile(file_path):
        return serialization.deserialize(file_path)
    else:
        return None


def load_dataset(train_test_ratio, percent_of_data=1.0):
    # data = pandas.read_csv(dataset_path)
    data = pandas.read_csv(
        dataset_path,
        header=0,
        skiprows=lambda i: i > 0 and random.random() > percent_of_data
    )

    # data.to_csv("../dataset/data_" + str(data_percent)+ ".csv", encoding='utf-8')

    processed_data = data_processor.preprocess(data)

    comments = [row[1] for row in processed_data]
    labels = [row[0] for row in processed_data]
    score = [row[2] for row in processed_data]
    ups = [row[3] for row in processed_data]
    downs = [row[4] for row in processed_data]
    parent = [row[5] for row in processed_data]

    train_set_size = int(len(comments) * train_test_ratio)

    train_data = DataModel(comments=comments[:train_set_size], labels=labels[:train_set_size],
                           score=score[:train_set_size],
                           ups=ups[:train_set_size], downs=downs[:train_set_size], parents=parent[:train_set_size])
    test_data = DataModel(comments=comments[train_set_size:], labels=labels[train_set_size:],
                          score=score[train_set_size:],
                          ups=ups[train_set_size:], downs=downs[train_set_size:], parents=parent[train_set_size:])

    return train_data, test_data


def load_word2vec_model(model_path, data):
    print("Loading word2vec model...")
    if os.path.isfile(model_path):
        try:
            model = gensim.models.Word2Vec.load(model_path)
        except:
            model = gensim.models.KeyedVectors.load_word2vec_format(model_path, binary=True)
    else:
        model = word2vec.train_word2vec(data, model_path)

    print("Vocabulary size: ", len(model.wv.vocab))
    return model, len(model.wv.vocab)


def save_data_model(data_model: DataModel, file_path):
    print("\nSaving data to file (serialize data model)")
    data_model.prepare_for_serialization()
    serialization.serialize(data_model, file_path)


def load_glove_model(model_path):
    model = dict()
    f = open(model_path)
    for line in f:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        model[word] = coefs
    f.close()
    print("Vocabulary size: ", len(model))
    return model, len(model)
