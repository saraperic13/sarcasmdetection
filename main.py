from sklearn import metrics
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
import loader
from data_model import DataModel


def init():
    train_data, test_data = loader.load_data()

    random_forest_classifier(train_data, test_data)
    # svm_classifier(train_data, test_data)
    # logistic_regression(train_data, test_data)


def random_forest_classifier(train_data: DataModel, test_data: DataModel):
    forest = RandomForestClassifier(n_estimators=1000, random_state=0, min_samples_split=16)

    print("Fitting random forest to training data....")
    forest = forest.fit(train_data.get_data(), train_data.labels)

    result = forest.predict(test_data.get_data())
    print(result)
    f1_score = calculate_f1_score(test_data.labels, result)
    accuracy = calculate_accuracy(test_data.labels, result)
    print(f1_score, accuracy)


def svm_classifier(train_data: DataModel, test_data: DataModel):
    svc = svm.SVC(kernel='rbf', degree=5, gamma=0.1, C=0.01)
    # svc = svm.LinearSVC(C=0.01)

    print("Fitting SVM to training data....")
    svc = svc.fit(train_data.get_data(), train_data.labels)

    result = svc.predict(test_data.get_data())
    print(result)
    f1_score = calculate_f1_score(test_data.labels, result)
    accuracy = calculate_accuracy(test_data.labels, result)
    print(f1_score, accuracy)


def logistic_regression(train_data: DataModel, test_data: DataModel):
    regression = LogisticRegression(random_state=0, solver='liblinear', multi_class='ovr')

    print("Fitting logistic regression to training data....")
    regression = regression.fit(train_data.get_data(), train_data.labels)

    result = regression.predict(test_data.get_data())
    print(result)
    f1_score = calculate_f1_score(test_data.labels, result)
    accuracy = calculate_accuracy(test_data.labels, result)
    print(f1_score, accuracy)


def calculate_f1_score(test_labels, prediction):
    return metrics.f1_score(test_labels, prediction, average='micro')


def calculate_accuracy(test_labels, prediction):
    return metrics.accuracy_score(test_labels, prediction)


if __name__ == "__main__":
    init()
