from gensim.models import word2vec

# Creating the model and setting values for the various parameters
num_features = 50  # Word vector dimensionality
min_word_count = 1  # Minimum word count
num_workers = 10  # Number of parallel threads
context = 10  # Context window size
downsampling = 1e-3  # (0.001) Downsample setting for frequent words


def train_word2vec(vocabulary, model_path):
    print("Training word2vec model on %d ...." %(len(vocabulary)))
    model = word2vec.Word2Vec(vocabulary, \
                              workers=num_workers, \
                              size=num_features, \
                              min_count=min_word_count, \
                              window=context,
                              sample=downsampling)

    # To make the model memory efficient
    model.init_sims(replace=True)
    model.train(vocabulary, total_examples=len(vocabulary), epochs=10)

    # Saving the model for later use. Can be loaded using Word2Vec.load()
    model.save(model_path)
    print("Model saved.")
    return model
