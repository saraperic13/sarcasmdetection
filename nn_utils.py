import matplotlib.pyplot as plt
from keras.callbacks import History
from data_model import DataModel
from nn_data_model import NNDataModel


def plot(hist: History):
    plt.figure()
    plt.plot(hist.history['loss'], lw=2.0, color='b', label='train')
    plt.plot(hist.history['val_loss'], lw=2.0, color='r', label='val')
    plt.title('CNN sarcasm detection')
    plt.xlabel('Epochs')
    plt.ylabel('Cross-Entropy Loss')
    plt.legend(loc='upper right')
    plt.show()

    plt.figure()
    plt.plot(hist.history['acc'], lw=2.0, color='b', label='train')
    plt.plot(hist.history['val_acc'], lw=2.0, color='r', label='val')
    plt.title('CNN sarcasm detection')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend(loc='upper left')
    plt.show()


def cast_data(train_data:DataModel, test_data:DataModel):

    embedding_weights, train_word_index = None, None
    if hasattr(train_data, 'word_vector_embedding_weights'):
        embedding_weights = train_data.word_vector_embedding_weights
    if hasattr(train_data, 'train_word_index'):
        train_word_index = train_data.train_word_index

    train_data= NNDataModel(labels=train_data.labels, comments=train_data.comments,
                            comments_vector=train_data.comments_vector,
                            parents=train_data.parents,
                            parents_vector=train_data.parents_vector,
                            word_vector_embedding_weights=embedding_weights,
                            train_word_index=train_word_index)

    test_data= NNDataModel(labels=test_data.labels,
                           comments=test_data.comments,
                           comments_vector=test_data.comments_vector,
                           parents=test_data.parents,
                           parents_vector=test_data.parents_vector)
    return train_data, test_data